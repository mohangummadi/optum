package com.optum.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.optum.base.entity.LoginUser;
import com.google.common.base.Optional;

public interface UserRepository extends JpaRepository<LoginUser, String>{

	Optional<LoginUser> findByUserName(String username);
	
}
