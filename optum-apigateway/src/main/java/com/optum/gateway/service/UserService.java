package com.optum.gateway.service;

import com.optum.base.entity.LoginUser;

public interface UserService {

	LoginUser findOne(String userName);

}
