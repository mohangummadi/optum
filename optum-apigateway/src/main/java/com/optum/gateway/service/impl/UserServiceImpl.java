package com.optum.gateway.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.optum.base.entity.LoginUser;
import com.optum.base.exception.UserNotValidException;
import com.optum.gateway.repository.UserRepository;
import com.optum.gateway.service.UserService;
import com.google.common.base.Optional;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, UserNotValidException {
		LoginUser user = findOne(username);
		UserDetails details = null;
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		details = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				grantedAuthorities);

		return details;
	}

	@Override
	public LoginUser findOne(String userName)throws UserNotValidException {
		Optional<LoginUser> user = userRepository.findByUserName(userName);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new UserNotValidException("Invalid Username or Password");
		}

	}

}
