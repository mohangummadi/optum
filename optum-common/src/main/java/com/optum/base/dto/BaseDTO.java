package com.optum.base.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author Mohan.Gummadi
 *
 * Jun 9, 2020
 */
@Data
public abstract class BaseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
}
