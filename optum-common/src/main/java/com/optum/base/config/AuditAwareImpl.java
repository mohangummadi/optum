package com.optum.base.config;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

public class AuditAwareImpl implements AuditorAware<String> {

	@Autowired
	private HttpServletRequest request;

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of("no_user");
	}

}
