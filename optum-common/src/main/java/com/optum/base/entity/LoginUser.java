package com.optum.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "je_td_user")
@Data
@EqualsAndHashCode(callSuper = false)
public class LoginUser extends BaseEntity {

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

}
