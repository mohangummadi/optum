package com.optum.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;


/**
 * @author Mohan.Gummadi
 *
 * Jun 9, 2020
 */
@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 352295862006555237L;
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "base_id", strategy = "com.blujay.base.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String id;

}
