package com.optum.base.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Mohan.Gummadi
 *
 * Jun 9, 2020
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode( callSuper = false)
public abstract class AuditEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@CreatedBy
	@Column(name = "created_by", nullable = false, updatable = false)
	private String createdBy;
	@LastModifiedBy
	@Column(name = "lastupdated_by")
	private String updatedBy;
	@CreatedDate
	@Column(name = "create_time", nullable = false, updatable = false)
	private Date createdDate;
	@LastModifiedDate
	@Column(name = "lastupdate_time")
	private Date updatedDate;

}
