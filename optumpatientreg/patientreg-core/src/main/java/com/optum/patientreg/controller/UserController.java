package com.blujay.hosman.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blujay.hosman.dto.UserDTO;
import com.blujay.hosman.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;

@Api(tags = "Save user")
@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping
	@ApiResponse(message = "User saved succesfully.", code = 200)
	public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO dto) {
		System.out.println(dto);
		return new ResponseEntity<UserDTO>(service.saveUser(dto), HttpStatus.OK);
	}
}
