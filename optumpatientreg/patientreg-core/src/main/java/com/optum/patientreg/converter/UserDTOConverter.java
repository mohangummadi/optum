package com.blujay.hosman.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.blujay.hosman.dto.UserDTO;
import com.blujay.hosman.entity.User;

@Service
public class UserDTOConverter {

	ModelMapper mapper = new ModelMapper();

	public User dtoToEntity(UserDTO dto) {
		return mapper.map(dto, User.class);
	}

	public UserDTO entityToDto(User user) {
		return mapper.map(user, UserDTO.class);
	}
}
