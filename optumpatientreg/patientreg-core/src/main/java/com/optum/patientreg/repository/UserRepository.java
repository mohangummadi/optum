package com.blujay.hosman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blujay.hosman.entity.User;

public interface UserRepository extends JpaRepository<User, String>{

}
