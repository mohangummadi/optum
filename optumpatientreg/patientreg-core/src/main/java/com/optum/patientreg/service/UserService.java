package com.blujay.hosman.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.blujay.hosman.converter.UserDTOConverter;
import com.blujay.hosman.dto.UserDTO;
import com.blujay.hosman.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserDTOConverter converter;

	@Autowired
	private PasswordEncoder encoder;

	public UserDTO saveUser(UserDTO userDto) {
		userDto.setPassword(encoder.encode(userDto.getPassword()));
		return converter.entityToDto(userRepository.save(converter.dtoToEntity(userDto)));
	}
}