package com.blujay.hosman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.blujay.base.entity.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Mohan.Gummadi
 *
 * Jun 11, 2020
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee extends AuditEntity {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "firstname")
	private String firstName;
	@Column(name = "lastname")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "phonenum")
	private String phoneNumber;
	@Column(name = "designation")
	private String designation;
	@Column(name = "department")
	private String department;

}
