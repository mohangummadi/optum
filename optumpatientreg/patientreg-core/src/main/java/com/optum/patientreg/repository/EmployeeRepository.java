package com.blujay.hosman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blujay.hosman.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>{

}
