package com.blujay.hosman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.blujay.base.config.AuditAwareImpl;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class HosmanpatientregCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(HosmanpatientregCoreApplication.class, args);
	}

	@Bean
	public AuditorAware<String> auditorAware() {
		return new AuditAwareImpl();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
