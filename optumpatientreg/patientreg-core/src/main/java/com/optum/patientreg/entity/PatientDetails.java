package com.blujay.hosman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.blujay.base.entity.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "patientdetails")
public class PatientDetails extends AuditEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "firstname")
	private String firstName;
	@Column(name = "lastname")
	private String lastName;
	@Column(name = "gender")
	private String gender;
	@Column(name = "doctorid")
	private String doctorid;
	@Column(name = "roomno")
	private String roomno;
	@Column(name = "admissiondate")
	private Date department;
	@Column(name = "dischargeddate")
	private Date dischargeddate;
	@Column(name = "admissiontype")
	private String admissionType;
}
