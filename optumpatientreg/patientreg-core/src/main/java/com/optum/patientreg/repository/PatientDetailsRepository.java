package com.blujay.hosman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blujay.hosman.entity.PatientDetails;

public interface PatientDetailsRepository extends JpaRepository<PatientDetails, String> {

}
