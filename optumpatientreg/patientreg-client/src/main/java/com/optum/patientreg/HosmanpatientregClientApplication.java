package com.blujay.hosman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HosmanpatientregClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(HosmanpatientregClientApplication.class, args);
	}

}
