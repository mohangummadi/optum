package com.blujay.hosman.dto;

import java.util.Date;

import com.blujay.base.dto.BaseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class PatientDetailsDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String gender;
	private String doctorid;
	private String roomno;
	private Date department;
	private Date dischargeddate;
	private String admissionType;
}
