package com.blujay.hosman.dto;

import com.blujay.base.dto.BaseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Mohan.Gummadi
 *
 * Jun 11, 2020
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String designation;
	private String department;

}
