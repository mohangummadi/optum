package com.blujay.hosman.dto;

import com.blujay.base.dto.BaseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Mohan.Gummadi
 *
 *         Jun 11, 2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserDTO extends BaseDTO {

	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private String role;
}
